# Les mises en service et les sorties {#mouvmts}

```{r, echo=FALSE, include=FALSE}
verbatim_3 <- creer_verbatim_3(data = indicateurs_rpls_ref, annee = annee)
```

## `r verbatim_3$intertitre` {.unnumbered}

`r verbatim_3$commentaire`


```{r MES par annee et par bailleur diag barres, fig.dim=c(5, 8)}
creer_graphe_3_1(data = indicateurs_rpls_ref, annee = annee)
```



```{r MES par type en et hors QPV}
creer_graphe_3_2(data = indicateurs_rpls_ref, annee = annee)
```


```{r, warning=FALSE, message=FALSE}
creer_tableau_3_1(data = indicateurs_rpls_ref, annee = annee, epci = FALSE)
```

**`r verbatim_3$encadre_titre`**

`r verbatim_3$encadre_paragraphe`



