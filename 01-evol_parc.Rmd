# L'évolution du parc {#evolparc}  


`r creer_verbatim_1(data = indicateurs_rpls_ref, annee = annee)`  




```{r, warning=FALSE, message=FALSE}

propre.rpls::creer_tableau_1_1(data = indicateurs_rpls_ref, annee = annee, epci = TRUE)

```


```{r evol parc graph 5 ans}
propre.rpls::creer_graphe_1_1(data = indicateurs_rpls_ref, annee = annee)
```


```{r evol parc carte, message=FALSE, warning=FALSE}
propre.rpls::creer_carte_1_1(indicateurs_rpls, carto = fond_carto, annee = annee)
```
