# Méthodologie et définitions {#methodo}

Le répertoire des logements locatifs des bailleurs sociaux a pour objectif de dresser l’état global du parc de logements locatifs de ces bailleurs sociaux au 1er janvier d’une année (nombre de logements, modifications intervenues au cours de l’année écoulée, localisation, taux d’occupation, mobilité, niveau des loyers, financement et conventionnement). Mis en place au 1er janvier 2011, il est alimenté par les informations transmises par les bailleurs sociaux.

Les bailleurs tenus à cette transmission sont :

- les organismes d’habitations à loyer modéré visés à l’article [L.411-2 du Code de la construction et de l’habitation (CCH)](https://www.legifrance.gouv.fr/codes/article_lc/LEGIARTI000038834254/2019-09-01) ;
- les sociétés d’économie mixte visées à l’article [L.481-1 du CCH](https://www.legifrance.gouv.fr/codes/article_lc/LEGIARTI000038834091) ;
- l’établissement public de gestion immobilière du Nord-Pas-de-Calais et la société anonyme Sainte-Barbe ;
- l’association foncière logement mentionnée à l’article [L.313-34 du CCH](https://www.legifrance.gouv.fr/codes/article_lc/LEGIARTI000037669426/) et les sociétés civiles immobilières dont les parts sont détenues pour au moins 99 % par cette association ;
- les organismes bénéficiant de l’agrément prévu à l’article [L.365-2 du CCH](https://www.legifrance.gouv.fr/codes/article_lc/LEGIARTI000020465508/2014-03-27).

Les informations transmises concernent les logements locatifs sur lesquels ces bailleurs sociaux sont titulaires d’un droit réel immobilier (pleine propriété ou bénéficiaire d’un bail emphytéotique, à réhabilitation ou à construction) ou usufruitiers.
Le répertoire ne couvre pas les logements-foyers de personnes âgées, de personnes handicapées, de jeunes travailleurs, de travailleurs migrants, ni les résidences sociales.

Chaque information transmise est relative à un logement locatif.
L’envoi dématérialisé du bailleur fait l’objet d’une analyse dont le but est de déterminer si la déclaration est cohérente avec les déclarations précédentes ou avec les informations connues sur son parc. Les résultats de cette analyse sont communiqués au bailleur.
Pour toute déclaration incomplète, incohérente ou ne respectant pas les formats techniques définis dans l’arrêté, un nouvel envoi est demandé.

Un identifiant est attribué à chaque logement par le ministère en charge de la gestion du répertoire lors de la première déclaration du logement. L’identifiant est signifié au bailleur en retour. L’identifiant appartient au logement, indépendamment du propriétaire. Celui-ci doit l’intégrer dans son système d’information et sa transmission est obligatoire lors des déclarations suivantes. En cas de transfert du logement à un autre bailleur social, il est inchangé et doit être transmis au nouveau propriétaire.

La transmission des informations pour la mise à jour du répertoire des logements locatifs est obligatoire :

- depuis le 1er janvier 2011 pour les organismes propriétaires d’au moins 1 000 logements au 1er janvier 2010 ;
- à compter du 1er janvier 2012 pour les autres organismes.

Le répertoire est mis à jour chaque année. Les organismes transmettent les informations sous forme dématérialisée (envoi de fichier(s)) avant le 1er mars. Les propriétaires de moins de 100 logements peuvent saisir les informations dans un formulaire disponible sur Internet.

A la fin du traitement du répertoire, une restitution personnalisée de la situation de son patrimoine est envoyée à chaque organisme.

**Parc locatif social** : le répertoire des logements locatifs des bailleurs sociaux (RPLS) recense l’ensemble des logements appartenant aux bailleurs sociaux. Le parc locatif social désigne l’ensemble des logements, conventionnés ou non, des bailleurs des catégories suivantes : OPH, ESH ou associations agréées. Le parc social comprend également les logements conventionnés des SEM agréées. Sont ainsi exclus les logements non conventionnés appartenant à une SEM de France métropolitaine dont les caractéristiques sont proches du secteur libre.

**Logement conventionné** : logement ayant fait l’objet d’une convention entre l’État et le propriétaire bailleur du logement.
Cette convention donne droit à l’aide personnalisée au logement (APL) pour les locataires qui remplissent les conditions d’octroi. Dans les DROM, le conventionnement à l’APL n’existe pas.

**Quartier prioritaire de la politique de la ville (QPV)** : la loi de programmation pour la ville et la cohésion urbaine a modifié la géographie prioritaire de la ville. Les quartiers prioritaires de la politique de la ville se sont substitués aux zones urbaines sensibles (ZUS) et aux quartiers en contrat urbain de cohésion sociale (CUCS) en janvier 2015. Ainsi, l’appartenance à un QPV a remplacé l’appartenance à une ZUS dans le répertoire au 1er janvier 2016.

**Nouvelles mises en service** : ensemble des logements mis en service entre le 2 janvier N-1 et le 1er janvier N. L’année de première mise en service est celle d’entrée du logement dans le parc locatif social.

**Parc locatif social récent** : ensemble des logements sociaux mis en service depuis 5 ans et moins. 

**Taux de vacance** : nombre de logements vacants parmi les logements proposés à la location, hors logements vides pour raison technique, rapporté au nombre de logements proposés à la location.

**Taux de vacance structurelle** : taux de logements vacants depuis plus de 3 mois.

**Taux de mobilité** : emménagements dans les logements proposés à la location depuis un an ou plus, rapportés au nombre de logements proposés à la location depuis un an ou plus. Les nouvelles mises en service ne sont pas intégrées dans le calcul de ce taux de mobilité. Les rotations au cours d’une même année ne sont pas mesurées. Un logement est considéré comme ayant fait l’objet d’un emménagement si le bail est en cours au 1er janvier de l'année N et a pris effet dans le courant de l’année N-1.

**Logements vides** : logements non occupés et non proposés à la location, car en cours ou en attente de travaux, de démolition ou de vente.

**Surface habitable** : surface de plancher construite, après déduction des surfaces occupées par les murs, cloisons, marches et cages d’escaliers, gaines, embrasures de portes et de fenêtres. Cette notion remplace celle de surface corrigée utilisée jusqu’en 2011.

**Loyer moyen (€/m²)** : somme des loyers divisée par la somme des surfaces habitables des logements loués au 1er janvier de l'année N.

**DPE** : le diagnostic de performance énergétique donne un aperçu de la performance d’un logement par une estimation de sa consommation énergétique et des émissions de gaz à effet de serre associées. Le DPE doit être réalisé dans tous les logements d’habitation, excepté ceux destinés à être occupés moins de quatre mois par an. Pour mesurer la performance énergétique d’un logement, le professionnel utilise deux étiquettes : une étiquette « énergie » indiquant la consommation énergétique annuelle du logement sur une échelle allant de A (consommation faible, inférieure à 51 kWh/m²/an) à G (consommation importante, supérieure à 450 kWh/m²/an), et une étiquette « effet de serre » indiquant l’impact annuel de cette consommation énergétique sur les émissions de gaz à effet de serre sur une échelle allant de A (émission faible, inférieure à 6 kg d’équivalent carbone/m²/an) à G (émission importante, supérieure à 80 kg d’équivalent carbone/m²/an). Dans cette publication, un DPE est considéré comme réalisé si le bailleur renseigne une date de réalisation de ce DPE. Toutefois certains bailleurs peuvent renseigner cette date sans remonter le résultat de ce diagnostic. Dans ce cas, nous n'avons pas d'étiquette pour ce DPE.

**Sorties de patrimoine** : les logements peuvent sortir du patrimoine d'un bailleur sociaux pour plusieurs raisons : 

* Ventes à l'occupant
* Autres Ventes
* Démolitions
* Transfert à un autre bailleur
* Changement d'usage du logement

Lors d'une fusion ou scission de logement, le bailleur reste propriétaire du patrimoine correspondant.

**Ventes** : ventes à l’occupant ou autres ventes.

**Rattrapages, erreurs et omissions** : écarts observés entre les déclarations des bailleurs sociaux entre l'année N-1 et N, tant vis-à-vis des sorties que des entrées dans le parc social. Ce peut être des rattrapages de collecte (logements non déclarés à tort dans les collectes précédentes mais réintégrés l'année N), des erreurs ou des omissions non repérées dans les déclarations des bailleurs sociaux.

**Modifications de structure** : changements d’usage du logement, logements disparus lors d’une fusion ou scission de logements.

**Sortie de champ** : bâtiments hors champ, déclarés à tort lors d’une collecte précédente (logements-foyers, commerces, parkings…).

**Financements** :

+ PLAI : Prêt locatif aidé d’intégration ;
+ PLUS : Prêt locatif à usage social. Les logements financés en Prêt locatif aidé ordinaire (PLA) appartiennent aussi à cette catégorie ;
+ PLS : Prêt locatif social ;
+ PLI : Prêt locatif intermédiaire.

Les regroupements selon les catégories de financement ci-dessus sont réalisés à partir du financement initial du logement. Pour retrouver la table de correspondance entre le financement initial et son équivalent en PLAI, PLUS, PLS ou PLI, vous pouvez consulter [la page 23 de la circulaire du 12 avril 2010 définissant les modalités d’élaboration et de suivi des CUS](http://www.financement-logement-social.logement.gouv.fr/IMG/pdf/cus_circulaire_12-avr-2010_cle152934.pdf).

La loi SRU (loi relative à la solidarité et au renouvellement) impose à certaines communes un taux minimum de logements sociaux (20 % ou 25 %). Pour atteindre cet objectif avec un bon équilibre de la production, la part des logements financés en PLS ne peut être supérieure à 30 % des logements sociaux à produire et celle des logements financés en PLAI est au moins égale à 30 %. Si la part des logements sociaux sur la commune est inférieure à 10 % du total des résidences principales et que la commune n'est pas couverte par un programme local de l'habitat, la part des logements financés en PLS ne peut être supérieure à 20 % des logements sociaux à réaliser.

**Organismes bailleurs** :

+ OPH : Organisme public de l’habitat ;
+ ESH : Entreprise sociale pour l’habitat ;
+ SEM : Société d’économie mixte.

**VEFA : vente en l'état futur d'achèvement** :
La vente en l'état futur d'achèvement est le contrat par lequel le vendeur transfère immédiatement à l'acquéreur ses droits sur le sol ainsi 
que la propriété des constructions existantes. Les ouvrages à venir deviennent la propriété de l'acquéreur au fur et à mesure de leur exécution ; 
l'acquéreur est tenu d'en payer le prix à mesure de l'avancement des travaux. Le vendeur conserve les pouvoirs de maître de l'ouvrage1 jusqu'à la 
réception des travaux. ([Article 1601-3 du Code Civil](https://www.legifrance.gouv.fr/codes/article_lc/LEGIARTI000006441481/2020-11-06))

**Attention** :

Certaines répartitions illustrées dans les graphiques ou tableaux peuvent s'écarter légèrement d'un total de 100 %, en raison des arrondis appliqués aux chiffres affichés.
